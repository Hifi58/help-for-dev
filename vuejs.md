# VUEJS

## Les directives

```v-text <nom de la variable>```: Permet d'afficher le contenu d'une variable dans un template. <br>💡 l'utilisation de ```{{<nom de la variable>}}``` est plus fiable.

```v-html```: Permet d'afficher du code html présent dans une variable au lieu de le transformer en String <br> ⚠ Attention aux failles XSS avec cette directive

```v-bind:<attribut natif html>="<nom de variable>"```: Permet de rendre dynamique le contenu des attributs html<br> 💡 l'utilisation de ```:<attribut natif html>="<nom de variable>"``` mène au même résultat

```v-on:<event js>="<fonction>"```: Permet la gestion d'événements<br> 💡```v-on``` peut être remplacé par ```@```