# Docker

## Notions de base

* Docker daemon : Process qui tourne en background sur la machine, et qui va attendre des instructions pour effectuer des actions. Docker daemon est pour Docker ce que le kernel est à l'os

* Docker CLI : Ensemble de commande dans le terminal qui vont permettre d'intéragir avec l'API du Docker daemon

* Docker File : Fichier dans lequelle vont être listé toutes les choses dont on a besoin pour faire touner notre application

* Docker image : Ensemble des dépendances écrite dans le Docker file et construite par le Docker daemon

* Docker container : Exécution d'une image

* Docker compose : Se charge d'organiser le travail entre les différents container

## Commandes

* ```docker container``` : Permet d'avoir la liste des commande disponible pour un conteneur

  * ```docker container create <nom de l'image>``` : Permet de créer un containeur à partir d'une image

    * ```docker container create -ai <nom de l'image>``` : Permet de créer un containeur à partir d'une image et de pouvoir interagir avec lui depuis un terminal

  * ```docker container start < du conteneur>``` : Permet de lancer un conteneur

  * ```docker container stop < du conteneur>``` : Permet de stopper un conteneur

  * ```docker container exec < nom du container> <commande terminal>``` : Permet d'executer une commande dans le conteneur sans passer par un terminal docker

  * ```docker container rename <nom actuel> <nouveau nom>``` : Permet de renommer un conteneur

  * ```docker container pause <nom du conteneur>``` : Permet de mettre en pause un conteneur

  * ```docker container unpause <nom du conteneur>``` : Permet de mettre d'enlever la pause un conteneur

  *  ```docker container ls -a``` : Permet de voir tous les conteneur et les images qu'ils utilisent ```docker ps -a``` est un alias à cette commande

  * ```docker container prune``` : Supprime automatiquement les conteneur qui ne sont pas en train de tourner

  * ```docker container kill < du conteneur>``` : Permet de stopper un conteneur de manière radicale si le conteneur est buguer et que la commande stop ne fonctionne pas

* ```docker run <nom de l'image>``` : Permet d'executer ```docker container create``` & ```docker container start``` en une seule commande

  * ```docker run -i <nom de l'image>```: Permet d'écrire des commande directement à l'intérieur du conteneur

  * ```docker run -it <nom de l'image>```: Permet d'écrire des commande directement à l'intérieur du conteneur à l'aide d'un terminal dédié qui rend l'affichage plus propre

  * ```docker run -p <port host>:<port container> <nom du container>```: Permet de lié un port de la machine à un port du conteneur

* ```docker image``` : Permet d'avoir la liste des commande disponible pour une image

  * ```docker image ls``` : liste les images locales ```docker images``` fait la même chose

  * ```docker image prune``` : Supprime les images qui ne sont pas utiliser par un conteneur en cours d'execution

* ```docker system prune -a``` : Supprime tous les conteneur, toutes les images, les caches, et les reseaux qui ne sont pas utiliser

## Dockerfile

Le Dockerfile permet la creation d'une image à partir d'une image de base à laquelle on va apporter des modification, et une action par défault au lancement.

[![](https://mermaid.ink/img/pako:eNo1zjEOgzAMBdCrRJ5AggtkqASlY6d2zOImDkQiCQrJUCHuXtNST5b9vuwNdDQEEsaEyySegwqCq6ucx5GEIfHClWrRthfRV_donHUas4uh_sn-u7pWnT6GYsHEIYtlzgygAU_JozN8YTsCCvJEnhRIbk-oQIWdaVkMZroZl2MCaXFeqQEsOT7eQYPMqdAfDQ75YX-q_QP9R0Cb)](https://mermaid.live/edit#pako:eNo1zjEOgzAMBdCrRJ5AggtkqASlY6d2zOImDkQiCQrJUCHuXtNST5b9vuwNdDQEEsaEyySegwqCq6ucx5GEIfHClWrRthfRV_donHUas4uh_sn-u7pWnT6GYsHEIYtlzgygAU_JozN8YTsCCvJEnhRIbk-oQIWdaVkMZroZl2MCaXFeqQEsOT7eQYPMqdAfDQ75YX-q_QP9R0Cb)

⚠ Le fichier Dockerfile doit toujours s'appeler Dockerfile

Appelle de l'image de base en faisant ```FROM <image>```

Installation de la nouvelle image à l'intérieur de celle appellé avec ```RUN apk add --update <image>```

Copie du fichier dans lequel on aura besoin de l'image dans un dossier ```COPY ./<nom du fichier> /<nom du dossier>/``` 💡 Si le dossier n'existe pas il sera créé

Configuration de l'action de base de l'image ```CMD ["<nom de l'image>", "<path du fichier qui utilise l'image>"]```

Une fois l'image construite on utilise ```docker build -t <nom de l'image>:version .``` pour construire l'image à partir du fichier Dockerfile dans le fichier courant

Ceci est un Dockerfile basique, il y a différente commande que l'on peut placer à l'intérieur pour l'agrémenter

### Commandes Dockerfile

* ```FROM <image depuis le hub>``` : Permet de récupérer l'image de base depuis le hub et va créer un conteneur depuis cette image

* ```RUN apk add --update <image à passer en argument>``` : Permet d'executer l'image en argument dans le conteneur créé précédement

* ```COPY ./<nom du fichier> /<nom du dossier>/``` : Permet de copier le fichier sources dans le dossier du conteneur

* ```WORKDIR /<nom du dossier>``` : Permet de spécifier dans le conteneur le dossier dans lequel l'image va être

* ```ARG <nom de la variable>``` : Permet de mettre en place des variables dans Docker qui seront définir dans la ligne de commande ```docker build --build-arg <nom de la variable>=<contenu de la variable>``` et utilisé dans le fichier avec ```$<nom de la variable>``` 💡on peut définir la variable directement dans le fichier ```ARG <nom de la variable> = <contenu de la variable>```

